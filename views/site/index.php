<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Selección 4</h1>

        <p class="lead">Módulo 3 - Unidad 2</p>
    </div>

    <div class="body-content">

        <div class="row">
            <!--  
            botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p>Nombre y edad de los ciclistas que han ganado etapas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!--  
            fin botón de consulta
            -->
            <!-- inicio consulta 2 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 2</h3>
                        <p>Nombre y edad de los ciclistas que han ganado puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 2 -->
            <!-- inicio consulta 3 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 3</h3>
                        <p>Nombre y edad de los ciclistas que han ganado etapas y puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 3 -->
            <!-- inicio consulta 4 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 4</h3>
                        <p>Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta4a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 4 -->
            <!-- inicio consulta 5 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 5</h3>
                        <p>Dorsal y nombre de los ciclistas que hayan llevado algún maillot</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta5a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 5 -->
            <!-- inicio consulta 6 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 6</h3>
                        <p>Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta6a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 6 -->
            <!-- inicio consulta 7 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 7</h3>
                        <p>Dorsal de los ciclistas que hayan llevado algún maillot y que hayan ganado etapas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta7a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 7 -->
            
            <!-- inicio consulta 8 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 8</h3>
                        <p>Indicar el numetapa de las etapas que tengan puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta8a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 8 -->
            
            <!-- inicio consulta 9 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 9</h3>
                        <p>Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta9a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 9 -->
            
            <!-- inicio consulta 10 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 10</h3>
                        <p>Listar el número de ciclistas que hayan ganado alguna etapa con puerto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta10a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 10 -->
            
            <!-- inicio consulta 11 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 11</h3>
                        <p>Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta11a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 11 -->
            
            <!-- inicio consulta 12 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 12</h3>
                        <p>Listar el número de las etapas que tengan puerto que hayan sido ganadas por ciclistas de Banesto con más de 200km o 200km</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta12a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta12'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 12 -->
            
            
        </div>

    </div>
</div>
