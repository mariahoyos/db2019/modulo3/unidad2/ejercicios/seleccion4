<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Puerto;
use yii\widgets\ListView;
use app\models\Equipo;
use app\models\Etapa;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCrud(){
        return $this->render("gestion");
    }
    
    public function actionConsulta1a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("nombre, edad")
                    ->distinct()
                    //->with('etapas'), con with hacemos la conexión pero no se ejecuta el join
                    ->joinWith('etapas', true, 'INNER JOIN'),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        //var_dump(Ciclista::find()->with('etapas')->all()[2]->etapas); prueba para usar with en vez de joinWith
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas",
            "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa USING (dorsal)",
        ]);
        
    }
    public function actionConsulta1(){
        
        /*$numero = Yii::$app->db
                ->createCommand('select count(*)cuenta from ciclista')
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa USING (dorsal)',
            'pagination'=>[
                'pageSize' => 6,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas",
            "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa USING (dorsal)",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("nombre, edad")
                    ->distinct()
                    ->joinWith('puertos', true, 'INNER JOIN'),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos",
            "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista JOIN puerto USING (dorsal)",
        ]);
        
    }
    
    public function actionConsulta2(){
        //mediante DAO
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(distinct edad) FROM ciclista WHERE nomequipo = 'Artiach'")
                ->queryScalar();*/
        
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT DISTINCT nombre, edad FROM ciclista JOIN puerto USING (dorsal)",
            //'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 6,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos",
            "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista JOIN puerto USING (dorsal)",
        ]);
    }
    
    public function actionConsulta3a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("nombre, edad")
                    ->distinct()
                    ->joinWith('puertos', false, 'INNER JOIN')
                    ->joinWith('etapas', false, 'INNER JOIN'),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos",
            "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa USING(dorsal) JOIN puerto USING(dorsal)",
        ]);
        
        /*echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'resultado1',
        ]);*/
        
    }
    
    public function actionConsulta3(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa USING(dorsal) JOIN puerto USING(dorsal)",
            //'totalCount' => $numero,
            /*'pagination' =>[
                'pageSize'=>5,
            ]*/
        ]);
        
        return $this->render('resultado',[
            "resultados" => $dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos",
            "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa USING(dorsal) JOIN puerto USING(dorsal)",
        ]);
            
        
        
    }
    
    public function actionConsulta4a(){
        //mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Equipo::find()
                    ->select("director")
                    ->distinct()
                    ->joinWith('ciclistas', false, 'INNER JOIN')
                    ->innerjoin('etapa', 'etapa.dorsal=ciclista.dorsal'),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        
        //$a=Equipo::find()->joinWith('ciclistas',false,'inner join')->innerjoin('etapa', 'etapa.dorsal=ciclista.dorsal')->all();
        //var_dump($a[0]->ciclistas);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa",
            "sql"=>"SELECT DISTINCT director FROM equipo JOIN ciclista USING(nomequipo) JOIN etapa USING(dorsal)",
        ]);
        
    }
    
    public function actionConsulta4(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE edad<24 OR edad>30")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT director FROM equipo JOIN ciclista USING(nomequipo) JOIN etapa USING(dorsal)",
            //'totalCount' =>$numero,
            'pagination'=>[
                'pageSize'=>5
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa",
            "sql"=>"SELECT DISTINCT director FROM equipo JOIN ciclista USING(nomequipo) JOIN etapa USING(dorsal)",
        ]);
    }
    
    public function actionConsulta5a(){
        //mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("l.dorsal, nombre")
                    ->distinct()
                    ->joinWith('llevas l', false, 'INNER JOIN'),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algún maillot",
            "sql"=>"SELECT DISTINCT dorsal, nombre FROM ciclista JOIN lleva USING(dorsal)",
        ]);
         
    }
    
    public function actionConsulta5(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND (nomequipo = 'Banesto')")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT dorsal, nombre FROM ciclista JOIN lleva USING(dorsal)",
            //'totalCount' =>$numero,
            'pagination'=>[
                'pageSize'=>5
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algún maillot",
            "sql"=>"SELECT DISTINCT dorsal, nombre FROM ciclista JOIN lleva USING(dorsal)",
        ]);
    }
    
    public function actionConsulta6a(){
    //por terminar    
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("l.dorsal, nombre")
                    ->distinct()
                    ->joinWith('llevas l', false, 'INNER JOIN')
                    ->innerJoin('maillot', 'maillot.código=l.código')
                    ->where("maillot.color='amarillo'"),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo",
            "sql"=>"SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal) JOIN maillot USING(código) WHERE color='amarillo'",
        ]);
        
    }
    
    public function actionConsulta6(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre) FROM  ciclista WHERE CHAR_LENGTH(nombre)>8")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal) JOIN maillot USING(código) WHERE color='amarillo'",
           //'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>6,
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo",
            "sql"=>"SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal) JOIN maillot USING(código) WHERE color='amarillo'",
        ]);
        
    }
    
    public function actionConsulta7a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                    ->select("lleva.dorsal")
                    ->distinct()
                    ->innerJoin('lleva', 'etapa.dorsal=lleva.dorsal'),
                    
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"Dorsal de los ciclistas que hayan llevado algún maillot y que hayan ganado etapas",
            "sql"=>"SELECT DISTINCT dorsal FROM etapa JOIN lleva USING(dorsal)",
            
        ]);
     
    }
    
    public function actionConsulta7(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT dorsal FROM etapa JOIN lleva USING(dorsal)",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>6
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Dorsal de los ciclistas que hayan llevado algún maillot y que hayan ganado etapas",
            "sql"=>"SELECT DISTINCT dorsal FROM etapa JOIN lleva USING(dorsal)",
        ]);
        
    }
    
    public function actionConsulta8a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                    ->select("puerto.numetapa")
                    ->distinct()
                    ->joinWith('puertos', false, 'inner join'),
                    
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Indicar el numetapa de las etapas que tengan puertos",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa JOIN puerto USING(numetapa)",
            
        ]);
     
    }
    
    public function actionConsulta8(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM lleva WHERE código = 'MGE'")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT etapa.numetapa FROM etapa JOIN puerto USING(numetapa)",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Indicar el numetapa de las etapas que tengan puertos",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa JOIN puerto USING(numetapa)",
        ]);
        
    }
    
    public function actionConsulta9a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->innerJoinWith('dorsal0',true) //cuando un modelo no tiene relación hecha con otro se usa el método que los enlaza
                ->innerJoinWith('puertos',true)
                ->where("nomequipo='Banesto'")
                ->select("kms,etapa.numetapa"),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['kms','numetapa'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos",
            "sql"=>"SELECT DISTINCT kms,etapa.numetapa FROM ciclista JOIN etapa ON ciclista.dorsal = etapa.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='banesto'",
            
        ]);
     
    }
    
    public function actionConsulta9(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT kms,etapa.numetapa FROM ciclista JOIN etapa ON ciclista.dorsal = etapa.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='banesto'",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>6
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['kms','numetapa'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos",
            "sql"=>"SELECT DISTINCT kms,etapa.numetapa FROM ciclista JOIN etapa ON ciclista.dorsal = etapa.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='banesto'",
        ]);
        
    }
    
    public function actionConsulta10a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select("count(distinct etapa.dorsal)cuenta")
                ->innerJoinWith('puertos', true),
//            'pagination'=>[
//                'pageSize'=>1,
//            ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['cuenta'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el número de ciclistas que hayan ganado alguna etapa con puerto",
            "sql"=>"SELECT COUNT(DISTINCT etapa.dorsal) FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa",
            
        ]);
     
    }
    
    public function actionConsulta10(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT COUNT(DISTINCT etapa.dorsal)cuenta FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['cuenta'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el número de ciclistas que hayan ganado alguna etapa con puerto",
            "sql"=>"SELECT COUNT(DISTINCT etapa.dorsal) FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa",
        ]);
        
    }
    
    public function actionConsulta11a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select('nombre')
                ->distinct()
                ->innerJoinWith('puertos', true)
                ->where("nomequipo='Banesto'")
                
                
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE nomequipo='Banesto'",
            
        ]);
     
    }
    
    public function actionConsulta11(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT nombre FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE nomequipo='Banesto'",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE nomequipo='Banesto'",
        ]);
        
    }
    
    public function actionConsulta12a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select('etapa.numetapa')
                ->distinct()
                ->innerJoinWith('dorsal0')
                ->innerJoinWith('puertos')
                ->where("nomequipo='Banesto'")
                ->andWhere("kms>=200")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Listar el número de las etapas que tengan puerto que hayan sido ganadas por ciclistas de Banesto con más de 200km o 200km",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='Banesto' AND kms>=200",
            
        ]);
     
    }
    
    public function actionConsulta12(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT etapa.numetapa FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='Banesto' AND kms>=200",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>1
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Listar el número de las etapas que tengan puerto que hayan sido ganadas por ciclistas de Banesto con más de 200km o 200km",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='Banesto' AND kms>=200",
        ]);
        
    }
    
    public function actionConsulta14a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("avg(altura)media")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 14 con Active Record",
            "enunciado"=>"Indicar la altura media de los puertos",
            "sql"=>"SELECT AVG(altura) FROM puerto",
            
        ]);
     
    }
    
    public function actionConsulta14(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT AVG(altura)media FROM puerto",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>1
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 14 con DAO",
            "enunciado"=>"Indicar la altura media de los puertos",
            "sql"=>"SELECT AVG(altura) FROM puerto",
        ]);
        
    }
    
    public function actionConsulta18a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()
                ->select("dorsal, código, count(*)cuenta")
                ->groupby("dorsal,código"),
            'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado1', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','cuenta'],
            "titulo"=>"Consulta 18 con Active Record",
            "enunciado"=>"Listar el dorsal del ciclista con el código de maillot y cuántas veces ese ciclista ha llevado ese maillot",
            "sql"=>"SELECT dorsal, código, COUNT(*) FROM lleva GROUP BY dorsal, código",
            
        ]);
        
     
    }
    
    public function actionConsulta18(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT dorsal, código, COUNT(*)cuenta FROM lleva GROUP BY dorsal, código",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','cuenta'],
            "titulo"=>"Consulta 18 con DAO",
            "enunciado"=>"Listar el dorsal del ciclista con el código de maillot y cuántas veces ese ciclista ha llevado ese maillot",
            "sql"=>"SELECT dorsal, código, COUNT(*) FROM lleva GROUP BY dorsal, código",
        ]);
        
    }
    
}
